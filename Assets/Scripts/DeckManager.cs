﻿using System.Collections.Generic;
using System.Linq;
using FMODUnity;
using MarkLight;
using MarkLight.Animation;
using MarkLight.Views.UI;
using RethinkDb.Driver;
using RethinkDb.Driver.Net;
using UnityEngine;

namespace AGC.CCG
{
    public class DeckManager : View
    {
        private const string UserName = "Edgar";
        private const string ButtonSoundEvent = "event:/UI/ButtonClick";
        private static readonly RethinkDB R = RethinkDB.R;

        #region CardPrefabs

        private readonly Card[] _cardsList =
        {
            new Card
            {
                Rank = CardRank.Normal,
                Name = "Card-1-0",
                ImagePath = "Assets/Cards/Normal/Card-0.png",
                Value = 5
            },
            new Card
            {
                Rank = CardRank.Normal,
                Name = "Card-1-0",
                ImagePath = "Assets/Cards/Normal/Card-1.png",
                Value = 5
            },
            new Card
            {
                Rank = CardRank.Normal,
                Name = "Card-1-0",
                ImagePath = "Assets/Cards/Normal/Card-2.png",
                Value = 5
            },
            new Card
            {
                Rank = CardRank.Normal,
                Name = "Card-1-0",
                ImagePath = "Assets/Cards/Normal/Card-3.png",
                Value = 5
            },
            new Card
            {
                Rank = CardRank.Normal,
                Name = "Card-1-0",
                ImagePath = "Assets/Cards/Normal/Card-4.png",
                Value = 5
            }
        };

        #endregion

        private CardFlipAnimationHelper _animationHelper;
        private Connection _connection;
        private User _user;
        private UserDao _userDao;

        public ObservableList<Card> BoosterPack = new ObservableList<Card>();
        public _int BoosterPackAmount;
        public _bool CanOpenPack;
        public _int Coins;
        public ViewSwitcher ContentViewSwitcher;
        public ObservableList<Card> Vault = new ObservableList<Card>();


        private void Awake()
        {
            _animationHelper = new CardFlipAnimationHelper();
            _connection = R.Connection()
                .Hostname("localhost")
                .Port(RethinkDBConstants.DefaultPort)
                .Timeout(60)
                .Connect();
        }

        private void Start()
        {
            _userDao = new UserDao(_connection);
            _user = _userDao.GetById(UserName);
            if (_user == null)
            {
                _user = new User
                {
                    Id = UserName,
                    BoosterPacks = new List<BoosterPack>(),
                    Vault = new List<Card>(),
                    Coins = 10
                };
                _userDao.SaveOrUpdateAsync(_user);
            }

            Vault.AddRange(_user.Vault);
            BoosterPackAmount.Value = _user.BoosterPacks.Count;
            CanOpenPack.Value = _user.BoosterPacks.Count != 0;
            Coins.Value = _user.Coins;

            Vault.ListChanged += (sender, args) =>
            {
                _user.Vault = Vault.ToList();
                _userDao.SaveOrUpdateAsync(_user);
            };
            Coins.ValueSet += (sender, args) =>
            {
                _user.Coins = Coins.Value;
                _userDao.SaveOrUpdateAsync(_user);
            };
        }


        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Delete) && Vault.SelectedItem != null)
                Sell();

            _animationHelper.Update();
        }


        private void BuyBoosterPack(BoosterPacksRank rank)
        {
            var bp = new BoosterPack
            {
                Rank = rank,
                Cards = new List<Card>(),
                Value = 5 + (int) rank * 2
            };

            for (var i = 0; i < 5; i++)
                bp.Cards.Add(_cardsList[Random.Range(0, _cardsList.Length - 1)]);


            if (Coins.Value - bp.Value < 0) return;
            RuntimeManager.PlayOneShot("event:/UI/BuyPack");
            Coins.Value -= bp.Value;
            _user.BoosterPacks.Add(bp);
            BoosterPackAmount.Value = _user.BoosterPacks.Count;
            _userDao.SaveOrUpdateAsync(_user);
            CanOpenPack.Value = _user.BoosterPacks.Count != 0;
        }

        private void Open()
        {
            RuntimeManager.PlayOneShot("event:/UI/OpenPack");
            BoosterPack.Clear();
            var boost = _user.BoosterPacks[0];
            Vault.AddRange(boost.Cards);
            BoosterPack.AddRange(boost.Cards);
            _user.BoosterPacks.Remove(boost);
            _userDao.SaveOrUpdateAsync(_user);
            BoosterPackAmount.Value = _user.BoosterPacks.Count;
            CanOpenPack.Value = _user.BoosterPacks.Count != 0;
        }


        private void Sell()
        {
            while (Vault.SelectedIndex != -1)
            {
                Coins.Value += Vault.SelectedItem.Value;
                Vault.Remove(Vault.SelectedItem);
            }

            RuntimeManager.PlayOneShot("event:/UI/Card");
        }


        private CardRank RandomRank(BoosterPacksRank rank)
        {
            var rn = Random.Range(0, 100);
            switch (rank)
            {
                case BoosterPacksRank.Gold:
                    rn -= 25;
                    break;
                case BoosterPacksRank.Silver:
                    rn -= 10;
                    break;
            }

            if (rn < 10)
                return CardRank.Epic;
            if (rn < 25)
                return CardRank.Rare;
            return CardRank.Normal;
        }

        #region ViewSwitcher

        public void ToMenu()
        {
            RuntimeManager.PlayOneShot(ButtonSoundEvent);
            ContentViewSwitcher.SwitchTo(0);
        }

        public void ToVault()
        {
            RuntimeManager.PlayOneShot(ButtonSoundEvent);
            ContentViewSwitcher.SwitchTo(1);
        }

        public void ToShop()
        {
            RuntimeManager.PlayOneShot(ButtonSoundEvent);
            ContentViewSwitcher.SwitchTo(2);
        }

        public void ToOpen()
        {
            RuntimeManager.PlayOneShot(ButtonSoundEvent);
            ContentViewSwitcher.SwitchTo(3);
        }

        public void ToBack()
        {
            RuntimeManager.PlayOneShot(ButtonSoundEvent);
            ContentViewSwitcher.Previous();
        }

        #endregion


        #region ButtonEvents

        private void BuyBoosterPack1()
        {
            BuyBoosterPack(BoosterPacksRank.Bronze);
        }

        private void BuyBoosterPack2()
        {
            BuyBoosterPack(BoosterPacksRank.Silver);
        }

        private void BuyBoosterPack3()
        {
            BuyBoosterPack(BoosterPacksRank.Gold);
        }


        private void PlayCardSound()
        {
            RuntimeManager.PlayOneShot("event:/UI/Card");
        }

        private void FlipCard(View source)
        {
            _animationHelper.Start(source);
        }

        #endregion
    }


    internal class CardFlipAnimationHelper
    {
        private readonly ViewFieldAnimator _flipCardAnimation;
        private bool _fliped;
        private View _view;

        public CardFlipAnimationHelper()
        {
            _flipCardAnimation = new ViewFieldAnimator
            {
                // set up animation
                Field = "Scale",
                From = new Vector3(1, 1),
                To = new Vector3(0, 1),
                Duration = 0.15f
            };
            _flipCardAnimation.Completed += Next;
        }

        private void Next(ViewFieldAnimator viewfieldanimator)
        {
            if (_fliped)
            {
                _fliped = false;
                _view = null;
                _flipCardAnimation.From = new Vector3(1, 1);
                _flipCardAnimation.To = new Vector3(0, 1);
                return;
            }

            _fliped = true;

            _view.GetChild(4).SetValue("IsVisible", false);

            _flipCardAnimation.From = new Vector3(0, 1);
            _flipCardAnimation.To = new Vector3(1, 1);
            _flipCardAnimation.StartAnimation();
        }

        public void Start(View source)
        {
            if (_view || !(bool) source.GetChild(4).GetValue("IsVisible")) return;
            _view = source;
            _view.GetChild(4).SetValue("IsVisible", true);
            _flipCardAnimation.TargetView = source;
            _flipCardAnimation.StartAnimation();
        }

        public void Update()
        {
            _flipCardAnimation.Update();
        }
    }
}