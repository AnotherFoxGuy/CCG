﻿using System.Collections.Generic;
using RethinkDb.Driver.Extras.Dao;
using RethinkDb.Driver.Net;

namespace AGC.CCG
{
    public enum BoosterPacksRank
    {
        Gold,
        Silver,
        Bronze
    }

    public enum CardRank
    {
        Normal,
        Rare,
        Epic
    }

    public class Card
    {
        public string ImagePath;
        public string Name;
        public CardRank Rank;
        public int Value;
    }

    public class BoosterPack
    {
        public List<Card> Cards;
        public BoosterPacksRank Rank;
        public int Value;
    }

    public class User : Document<string>
    {
        public List<BoosterPack> BoosterPacks;
        public int Coins;
        public List<Card> Vault;
    }

    public class UserDao : RethinkDao<User, string>
    {
        public UserDao(IConnection conn) : base(conn, "CCG", "Users")
        {
        }
    }
}